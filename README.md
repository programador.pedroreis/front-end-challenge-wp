# Front-End WordPress Developer - Challenge

Este desafio tem como objectivo avaliar suas habilidades como desenvolvedor Front-End com WordPress, jQuery, CSS, HTML e PHP.

## O Desafio

![Preview](preview.png)

Seu desafio será montar um **tema** simples para um blog WordPress, incluindo:
 
- Uma listagem de posts na página inicial, trazendo todas as postagens.
- Uma página interna com o conteúdo da postagem.
- Um menu de categorias para filtrar os posts na página inicial.

## Requisitos

- Seguir o design e briefing propostos;
- Utilizar **jQuery** para a funcionalidade do filtro;
- Escrever tanto o código quanto os comentários em inglês (se possível).

## Recursos

Na pasta `resources` temos alguns materiais que podem/devem ser utilizados no desenvolvimento do seu tema, esses materiais são:

- 1 exportação do WordPress (usando o plugin [All-In-One WP Migration](https://br.wordpress.org/plugins/all-in-one-wp-migration/)) já com os posts e categorias cadastradas e algumas pré-definições;
- 6 imagens do design desktop/mobile;
- 1 imagem com algumas especificações do design.

## Páginas

As páginas devem seguir as regras e funcionalidades definidas a seguir:

### Página inicial

A página inicial deve trazer todos os posts cadastrados no CMS, incluindo seu título, imagem destacada e link para a página da postagem.

O componente de filtro deve listar apenas as categorias que contém postagens, e incluir um botão que represente o estado inicial (a opção `Todos`).

Ao clicar em uma categoria/filtro, a lista de posts deve ser atualizada mostrando apenas os posts daquela categoria.

Os filtros devem ser aditivos, possibilitando mais de uma categoria filtrada ao mesmo tempo, com excessão da opção "**Todos**" que deve desabilitar as demais ao ser ativada (e vice-versa).

Ao clicar no link de uma postagem (título, imagem ou bloco inteiro), o visitante deve ser redirecionado para a página do artigo.

### Interna

A página da postagem deve conter as informações básicas do post (como mostrado no design) e ter um link para voltar para a Página Inicial.

## Setup Inicial

Para utilizar o mesmo conteúdo que será usado na avaliação, e economizar tempo, faça a importação do arquivo `export.wpress` disponibilizado na página de recursos.

Recomendamos seguir os passos abaixo:

1. Faça uma instalação limpa do WordPress na sua máquina local;
2. Instale e ative o plugin [All-In-One WP Migration](https://br.wordpress.org/plugins/all-in-one-wp-migration/);
3. Faça a importação do arquivo em `All-In-One WP Migration` > `Import`, esse arquivo contém o banco de dados com os posts e categorias cadastrados, e suas respectivas informações e imagens.
4. Instale e ative o seu tema.

## Enviando seu projeto

1. Efetue um **fork** deste repositório e crie um branch com o seu nome e sobrenome (ex: `joao-neves`);
2. Na raíz do projeto, coloque a pasta do seu `tema`, com o mesmo nome da sua branch;
3. Após finalizar o desafio, crie um **pull request** da sua branch;
4. Aguarde um avaliador revisar sua implementação;

## O que será avaliado?

- Organização e simplicidade do código;
- Fidelidade ao design e briefing;
- Responsividade;
- Lógica utilizada;
- SEO;
- Acessibilidade;

## Informações adicionais

Sinta-se livre para usar qualquer framework CSS de sua preferência, como _Bootstrap_, _Foundation_, ou outro, assim como pré-processadores, como _Sass_, _Less_ ou _Stylus_.

Construa a aplicação pensando no usuário final, e considerando futuras manutenções por você ou qualquer outro desenvolvedor no time.

Boa sorte! 😉